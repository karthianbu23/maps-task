import React from 'react';
import { dataOne } from './data';

class TaskThree extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            finalList: []
        }
    }

    componentDidMount() {
        this.getLastState()
    }

    getLastState = () => {
        const { finalList } = this.state
        for (var property in dataOne().States) {
            const stateLength = dataOne().States[property].length
            const newObj = {
                country: property,
                stateName: dataOne().States[property][stateLength - 1]
            }
            finalList.push(newObj)
        }
        this.setState({ finalList })
    }

    render() {
        const { finalList } = this.state
        return (
            <div className="task-inner-view">
                <div className="task-name">Use Data 1</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Last State</th>
                        </tr>
                    </thead>
                    <tbody>
                        {finalList.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.country}</td>
                                    <td>{list.stateName}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TaskThree;
