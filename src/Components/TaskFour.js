import React from 'react';
import { dataOne, dataTwo } from './data';

class TaskFour extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            finalList: [],
            withoutDuplicates: [],
        }
    }

    componentDidMount() {
        this.mergeStates()
    }

    mergeStates = () => {
        let firstData = dataOne().States
        let newArray = []
        let noDupArray = []
        for (var propertyOne in firstData) {
            for (var propertyTwo in dataTwo()) {
                if (propertyOne === propertyTwo) {
                    const newObj = {
                        country: propertyOne,
                        states: firstData[propertyOne].concat(dataTwo()[propertyTwo])
                    }
                    const withoutDup = {
                        country: propertyOne,
                        states: this.arrayUnique(firstData[propertyOne].concat(dataTwo()[propertyTwo]))
                    }
                    newArray.push(newObj)
                    noDupArray.push(withoutDup)
                }
            }
        }
        this.setState({ finalList: newArray, withoutDuplicates: noDupArray });
    }

    arrayUnique = (array) => {
        var a = array.concat();
        for (var i = 0; i < a.length; ++i) {
            for (var j = i + 1; j < a.length; ++j) {
                if (a[i] === a[j])
                    a.splice(j--, 1);
            }
        }

        return a;
    }

    render() {
        const { finalList, withoutDuplicates } = this.state
        return (
            <div className="task-inner-view">
                <div className="task-name">Use Data 1 and Data 2</div>
                <div style={{ marginTop: '10px', marginBottom: '10px', fontWeight: 'bold', color: 'blue' }}>Merge States with Duplicates</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Merge States</th>
                        </tr>
                    </thead>
                    <tbody>
                        {finalList.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.country}</td>
                                    <td>{list.states.join(',')}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div style={{ marginTop: '10px', marginBottom: '10px', fontWeight: 'bold', color: 'blue' }}>Merge States without Duplicates (Task Five)</div>
                <div className="task-name">From the output of task 4 remove the duplicate values in array from the all key value pairs of the state object</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Merge States</th>
                        </tr>
                    </thead>
                    <tbody>
                        {withoutDuplicates.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.country}</td>
                                    <td>{list.states.join(',')}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TaskFour;
