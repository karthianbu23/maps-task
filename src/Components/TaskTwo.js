import React from 'react';
import { dataOne } from './data';

class TaskTwo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            finalList: []
        }
    }

    componentDidMount() {
        this.getFirstState()
    }

    getFirstState = () => {
        const { finalList } = this.state
        for (var property in dataOne().States) {
            const newObj = {
                country: property,
                stateName: dataOne().States[property][0]
            }
            finalList.push(newObj)
        }
        this.setState({ finalList })
    }

    render() {
        const { finalList } = this.state
        return (
            <div className="task-inner-view">
                <div className="task-name">Use Data 1</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>First State</th>
                        </tr>
                    </thead>
                    <tbody>
                        {finalList.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.country}</td>
                                    <td>{list.stateName}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TaskTwo;
