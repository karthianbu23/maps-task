import React from 'react';
import '../App.css';
import TaskOne from './TaskOne';
import TaskTwo from './TaskTwo';
import TaskThree from './TaskThree';
import TaskFour from './TaskFour';
import TaskFive from './TaskFive';
import { dataOne, dataTwo, dataThree } from './data';
import TaskSix from './TaskSix';

class TaskConatiner extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            taskList: [
                { taskID: 1, taskHeader: 'Task One', taskName: "Explain the logic to retrieve the states of the country with country code as input from data1" },
                { taskID: 2, taskHeader: 'Task Two', taskName: "Using data 1  parse the data to retrieve the first value from array for each key in States object" },
                { taskID: 3, taskHeader: 'Task Three', taskName: "Using data 1  parse the data to retrieve the last value from array for each key in States object " },
                { taskID: 4, taskHeader: 'Task Four', taskName: " Merge the values from input data 2 to data 1 according to the key’s they match within the state object of data 1 and display the final object" },
                { taskID: 5, taskHeader: 'Task Five', taskName: "1. Retrive the country with highest and lowest population" },
                { taskID: 6, taskHeader: 'Task Six', taskName: "Drag and Drop" },
            ],
            currentTask: 1
        }
    }

    onClickNext = () => {
        let { currentTask } = this.state
        if (currentTask > 0 && currentTask < 6) {
            this.setState({ currentTask: currentTask + 1 })
        }
    }

    onClickPrev = () => {
        let { currentTask } = this.state
        if (currentTask > 1 && currentTask < 7) {
            this.setState({ currentTask: currentTask - 1 })
        }
    }

    openData = (json) => {
        var myjson = JSON.stringify(json, null, 2);
        var x = window.open();
        x.document.open();
        x.document.write('<html><body><pre>' + myjson + '</pre></body></html>');
        x.document.close();
    }

    render() {
        const { taskList, currentTask } = this.state
        const currentElement = taskList.find(element => { return element.taskID === currentTask })
        return (
            <div className="task-container">
                <div className="task-header">
                    {currentElement.taskHeader}
                    {currentTask !== 6 && <div className="datas">
                        <div className="dataItem" onClick={() => this.openData(dataOne())}>Data 1</div>
                        <div className="dataItem" onClick={() => this.openData(dataTwo())}>Data 2</div>
                        <div className="dataItem" onClick={() => this.openData(dataThree())}>Data 3</div>
                    </div>}
                </div>

                <div className="task-view">
                    <div className="task-name">
                        {currentTask !== 5 && currentElement.taskName}
                    </div>
                    <div className="task-show">
                        {currentTask === 1 && <TaskOne />}
                        {currentTask === 2 && <TaskTwo />}
                        {currentTask === 3 && <TaskThree />}
                        {currentTask === 4 && <TaskFour />}
                        {currentTask === 5 && <TaskFive />}
                        {currentTask === 6 && <TaskSix />}
                    </div>
                </div>
                <div className="task-buttons">
                    <div style={{ cursor: currentTask === 1 ? 'not-allowed' : 'pointer' }} className="prev-button" onClick={() => this.onClickPrev()}>Previous</div>
                    <div style={{ cursor: currentTask === 6 ? 'not-allowed' : 'pointer' }} className="next-button" onClick={() => this.onClickNext()}>Next</div>
                </div>
            </div >
        );
    }
}

export default TaskConatiner;
