import React from 'react';
import { dataThree } from './data';

class TaskFive extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            highestPopulationCountry: '',
            lowestPopulationCountry: '',
            highestPopulation: 0,
            lowestPopulation: 0,
            highestMalePopulationCountry: '',
            highestMalePopulation: 0,
            lowestMalePopulationCountry: '',
            lowestMalePopulation: 0,
            maleFemaleRatio: [],
            vehiclesPerPerson: [],
            vehiclesPercentBySector: []
        }
    }

    componentDidMount() {
        this.getValues()
    }

    getRatio = (a, b) => {
        return (b == 0) ? a : this.getRatio(b, a % b);
    }

    getPercentage = (num, per) => {
        return (per / num) * 100
    }


    getValues = () => {
        const dataToCheck = dataThree()

        // Highest Population Country
        let highestPopulation = 0;
        let highestPopulationCountry = ''
        dataToCheck.forEach(character => {
            if (character.population > highestPopulation) {
                highestPopulationCountry = character.country;
                highestPopulation = character.population
            }
        });

        // Lowest Population Country
        let lowestPopulationCountry = ''
        let lowestPopulation = highestPopulation
        dataToCheck.forEach(character => {
            if (character.population < lowestPopulation) {
                lowestPopulationCountry = character.country;
                lowestPopulation = character.population
            }
        });

        // Highest Male Population Country
        let highestMalePopulationCountry = ''
        let highestMalePopulation = 0
        dataToCheck.forEach(character => {
            if (character.populationbygender[0].male > lowestPopulation) {
                highestMalePopulationCountry = character.country;
                highestMalePopulation = character.populationbygender[0].male
            }
        });

        // Lowest Male Population Country
        let lowestMalePopulationCountry = ''
        let lowestMalePopulation = highestMalePopulation
        dataToCheck.forEach(character => {
            if (character.populationbygender[0].male < lowestMalePopulation) {
                lowestMalePopulationCountry = character.country;
                lowestMalePopulation = character.populationbygender[0].male
            }
        });


        // Male Female Ratio 
        const maleFemaleRatio = dataToCheck.map((eachItem) => {
            const dividend = this.getRatio(eachItem.populationbygender[0].male, eachItem.populationbygender[1].female)
            const newObj = {
                countryName: eachItem.country,
                ratio: `${eachItem.populationbygender[0].male / dividend}` + ':' + `${eachItem.populationbygender[1].female / dividend}`
            }
            return newObj
        })

        // Vehicles Per Person Ratio 
        const vehiclesPerPerson = dataToCheck.map((eachItem) => {
            const newObj = {
                countryName: eachItem.country,
                ratio: `${eachItem.population / eachItem.TotalVechilescount}`
            }
            return newObj
        })

        // Vehicles Percentage by sector 
        const vehiclesPercentBySector = dataToCheck.map((eachItem) => {
            const newObj = {
                countryName: eachItem.country,
                publicPercentage: this.getPercentage(eachItem.TotalVechilescount, eachItem.Vechilecountbysector[0].Public),
                privatePercentage: this.getPercentage(eachItem.TotalVechilescount, eachItem.Vechilecountbysector[1].Private),
                otherPercentage: this.getPercentage(eachItem.TotalVechilescount, eachItem.Vechilecountbysector[2].others)

            }
            return newObj
        })
        this.setState({
            highestPopulationCountry,
            highestPopulation,
            lowestPopulationCountry,
            lowestPopulation,
            highestMalePopulationCountry,
            highestMalePopulation,
            lowestMalePopulationCountry,
            lowestMalePopulation,
            maleFemaleRatio,
            vehiclesPerPerson,
            vehiclesPercentBySector
        })

    }

    render() {
        const {
            highestPopulationCountry,
            highestPopulation,
            lowestPopulationCountry,
            lowestPopulation,
            highestMalePopulationCountry,
            highestMalePopulation,
            lowestMalePopulationCountry,
            lowestMalePopulation,
            maleFemaleRatio,
            vehiclesPerPerson,
            vehiclesPercentBySector
        } = this.state
        const publicvehiclesChina = vehiclesPercentBySector.length !== 0 && vehiclesPercentBySector.find(element => { return element.countryName === 'China' })
        return (
            <div className="task-inner-view">
                <div className="task-name">Use Data 3</div>
                <div className="task-name">1.Retrive the country with highest and lowest population</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country With Highest Population</th>
                            <th>Country With Lowest Population</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{highestPopulationCountry}({highestPopulation})</td>
                            <td>{lowestPopulationCountry}({lowestPopulation})</td>
                        </tr>
                    </tbody>
                </table>
                <div className="task-name">2. Retrive the country with highest male and lowest male population</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country With Highest Male Population</th>
                            <th>Country With Lowest Male Population</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{highestMalePopulationCountry}({highestMalePopulation})</td>
                            <td>{lowestMalePopulationCountry}({lowestMalePopulation})</td>
                        </tr>
                    </tbody>
                </table>
                <div className="task-name">3. Calculate the ratio of male and female in each country and display the ratio along with country name</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Population Ratio (Male:Female)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {maleFemaleRatio.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.countryName}</td>
                                    <td>{list.ratio}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="task-name">4. Calculate the ratio of vehicles per person</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Vehicles Per Person</th>
                        </tr>
                    </thead>
                    <tbody>
                        {vehiclesPerPerson.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.countryName}</td>
                                    <td>{list.ratio}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="task-name">5. Calculate the percentage of share by each type of vehicles(public,private,others)</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Vehicles Percentage by Sector(Public, Private, Others)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {vehiclesPercentBySector.map((list, index) => {
                            return (
                                <tr key={index}>
                                    <td>{list.countryName}</td>
                                    <td>{list.publicPercentage}, {list.privatePercentage}, {list.otherPercentage}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <div className="task-name">6. Calculate the percentage of public vehicles wrt to total vehicles for country china</div>
                <table>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>Public Vehicles Percentage wrt Total Vehicles in China</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>China</td>
                            <td>{publicvehiclesChina.publicPercentage}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default TaskFive;
