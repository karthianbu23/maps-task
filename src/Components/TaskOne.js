import React from 'react';
import { dataOne } from './data';

class TaskOne extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: undefined,
            countryName: '',
            stateName: []
        }
    }

    getStates = (value) => {
        dataOne().CountryCode.map((countryCode) => {
            for (var property in countryCode) {
                if (value === property) {
                    this.setState({
                        countryName: countryCode[property],
                        stateName: dataOne().States[countryCode[property]]
                    })
                }
                else if (this.state.countryName !== '') {
                    this.setState({
                        countryName: '',
                        stateName: []
                    })
                }
            }
        })
    }

    render() {
        const { inputValue, countryName, stateName } = this.state
        return (
            <div className="task-inner-view">
                <div className="task-name">Use Data 1</div>
                <div className="input-field">
                    <div className="label">Country Code</div>
                    <input placeholder="Enter Country Code" className="field-style" value={inputValue} onChange={(e) => this.getStates(e.target.value)} />
                </div>
                <table style={{ marginBottom: '0px' }}>
                    <thead>
                        <tr>
                            <th>Country Name</th>
                            <th>States</th>
                        </tr>
                    </thead>
                    {countryName !== '' && <tbody>
                        <tr>
                            <td>{countryName}</td>
                            <td>{stateName.join()}</td>
                        </tr>
                    </tbody>}
                </table>
                {countryName === '' && <div className="nodata">No Data</div>}
            </div>
        );
    }
}

export default TaskOne;
