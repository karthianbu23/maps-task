import React from 'react';

class TaskSix extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: {
                inputFirstValue: '',
                inputSecondValue: '',
                inputThirdValue: '',
                inputFourthValue: '',
                inputFifthValue: '',
                inputSixthValue: ''
            },
            inputDragValue: ['Comma', 'Semicolon', 'Variable', 'Comments', 'Input', '$']
        }
    }

    onDragStartFun = (e, id) => {
        e.dataTransfer.setData('text/plain', id)
    }

    onDropFun = (e, key) => {
        const { inputDragValue, inputValue } = this.state
        let dropValue = e.dataTransfer.getData("text");
        if (key === 'inputFirstValue' && dropValue === 'Comma') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        } else if (key === 'inputSecondValue' && dropValue === 'Semicolon') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        } else if (key === 'inputThirdValue' && dropValue === 'Variable') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        } else if (key === 'inputFourthValue' && dropValue === 'Comments') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        } else if (key === 'inputFifthValue' && dropValue === 'Input') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        } else if (key === 'inputSixthValue' && dropValue === '$') {
            inputValue[key] = dropValue
            const index = inputDragValue.indexOf(dropValue);
            if (index > -1) {
                inputDragValue.splice(index, 1);
            }
        }
        this.setState({ inputValue, inputDragValue })
    }

    onDragOverFun = (e) => {
        e.preventDefault();
    }

    updateState = (key, value) => {
        const { inputValue } = this.state
        inputValue[key] = value
        this.setState({ inputValue })
    }

    onShow = () => {
        let { inputDragValue, inputValue } = this.state
        inputValue = {
            inputFirstValue: 'Comma',
            inputSecondValue: 'Semicolon',
            inputThirdValue: 'Variable',
            inputFourthValue: 'Comments',
            inputFifthValue: 'Input',
            inputSixthValue: '$',
        }
        inputDragValue = []
        this.setState({ inputValue, inputDragValue })
    }

    onReset = () => {
        let { inputDragValue, inputValue } = this.state
        inputValue = {
            inputFirstValue: '',
            inputSecondValue: '',
            inputThirdValue: '',
            inputFourthValue: '',
            inputFifthValue: '',
            inputSixthValue: '',
        }
        inputDragValue = ['Comma', 'Semicolon', 'Variable', 'Comments', 'Input', '$']
        this.setState({ inputValue, inputDragValue })
    }

    render() {
        const { inputValue, inputDragValue } = this.state
        return (
            <div className="task-inner-view">
                <div className="top-buttons">
                    <div className="show-button" onClick={() => this.onShow()}>Show Answers</div>
                    <div className="reset-button" onClick={() => this.onReset()}>Reset All</div>
                </div>
                <div className="drag-drop-head">
                    Fill the answers using drag and drop from the Help box
                </div>
                <div className="questions">
                    1. <input className="dropLocation" value={inputValue.inputFirstValue} onChange={(e) => this.updateState('inputFirstValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputFirstValue')} onDragOver={(e) => this.onDragOverFun(e)} /> and
                    <input className="dropLocation" value={inputValue.inputSecondValue} onChange={(e) => this.updateState('inputSecondValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputSecondValue')} onDragOver={(e) => this.onDragOverFun(e)} />
                    are the two separators used with PRINT statement.
                </div>

                <div className="questions">
                    2. <input className="dropLocation" value={inputValue.inputThirdValue} onChange={(e) => this.updateState('inputThirdValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputThirdValue')} onDragOver={(e) => this.onDragOverFun(e)} />
                    is a name of the menory location.
                </div>

                <div className="questions">
                    3. <input className="dropLocation" value={inputValue.inputFourthValue} onChange={(e) => this.updateState('inputFourthValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputFourthValue')} onDragOver={(e) => this.onDragOverFun(e)} />
                    are non-executable statements.
                </div>

                <div className="questions">
                    4. To accept a value from the user,
                    <input className="dropLocation" value={inputValue.inputFifthValue} onChange={(e) => this.updateState('inputFifthValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputFifthValue')} onDragOver={(e) => this.onDragOverFun(e)} />
                    statement is required.
                </div>

                <div className="questions">
                    5. <input className="dropLocation" value={inputValue.inputSixthValue} onChange={(e) => this.updateState('inputSixthValue', e.target.value)} onDrop={(e) => this.onDropFun(e, 'inputSixthValue')} onDragOver={(e) => this.onDragOverFun(e)} />
                    sign is used with string variables.
                </div>

                <div className="help-box">
                    <div className="help-head">Help Box</div>
                    <div className="help-box-container">
                        {inputDragValue.map((dragValues, index) => {
                            return (
                                <div
                                    draggable
                                    onDragStart={(e) => this.onDragStartFun(e, dragValues)}
                                    onDragOver={(e) => this.onDragOverFun(e)}
                                    key={index}
                                    className="drag-values">
                                    {dragValues}
                                </div>
                            )
                        })}
                    </div>
                </div>
            </div>
        );
    }
}

export default TaskSix;
