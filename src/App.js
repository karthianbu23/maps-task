import React from 'react';
import './App.css';
import TaskConatiner from './Components/TaskContainer';

class App extends React.Component {

  render() {
    return (
      <div className="main-container" >
        <div className="inner-container">
          <TaskConatiner/>
        </div>
      </div>
    );
  }
}

export default App;
